
let sorting = (array) => {
	array.sort(function(a, b) {
	  return a - b;
	});
	return array;
}

let compare = (a, b) => {
	return a['PM2.5'] -b['PM2.5'];
}



let average = (nums) => {
	function mean(numbers) {
		var total = 0, i;
		for (i = 0; i < numbers.length; i += 1) {
			total += numbers[i];
		}
		return total / numbers.length;
	}
	return Math.round(mean(nums)*100)/100;
}


module.exports = {
	sorting,
	compare,
	average
}